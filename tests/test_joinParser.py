# -*- coding: utf-8 -*-
from unittest import TestCase
from qparser.operation_parsers import JoinParser
from qnode.join import JoinNode
from qnode.table import TableNode
from db_exceptions import BadParameters

__author__ = 'Propp'

class TestJoinParser(TestCase):
    def setUp(self):
        self.p = JoinParser()

    def test_parse_correct(self):
        args = '(r1, r2)'

        root = JoinNode()
        root.lft = TableNode('r1')
        root.rht = TableNode('r2')

        self.assertEqual(root, self.p.parse(args))
        
    def test_parse_same_relations(self):
        args = '(r1, r1)'
        
        with self.assertRaises(BadParameters):
            self.p.parse(args)