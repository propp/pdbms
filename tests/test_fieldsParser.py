# -*- coding: utf-8 -*-
from unittest import TestCase
from db_exceptions import BadParameters
from qparser.fields_parser import FieldsParser

__author__ = 'Propp'

class TestFieldsParser(TestCase):
    def setUp(self):
        self.p = FieldsParser()

    def test_parse_correct_parameters(self):
        f = 'r.a, r1.a1, r1.a3'

        self.assertListEqual(['r.a', 'r1.a1', 'r1.a3'], self.p.parse(f))

    def test_parse_empty_string(self):
        f = '  '

        with self.assertRaises(BadParameters):
            self.p.parse(f)

    def test_parse_all_fields(self):
        f = ' * '

        self.assertIsNone(self.p.parse(f))

    def test_parse_incorrect_parameters(self):
        f = 'ra0, r.a1'

        with self.assertRaises(BadParameters):
            self.p.parse(f)
