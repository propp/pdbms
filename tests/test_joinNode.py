'''
Created on 02.11.2012

@author: Propp
'''
import unittest
from qnode.join import JoinNode
from qnode.table import TableNode

class Test(unittest.TestCase):
    def setUp(self):
        self.p = JoinNode()
        self.p.lft = TableNode('r1')
        self.p.rht = TableNode('r2')


    def tearDown(self):
        pass


    def testCorrect(self):
        for t in self.p:
            print t.values


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testCorrect']
    unittest.main()