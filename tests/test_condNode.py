#-*- coding: utf-8 -*-
'''
Created on 02.11.2012

@author: Propp
'''
import unittest
from qnode.condition import ConditionNode
from qnode.table import TableNode


class Test(unittest.TestCase):


    def setUp(self):
        self.p = ConditionNode("r1.a1 > 3")
        self.p.subnode = TableNode("r1")


    def tearDown(self):
        pass


    def testCorrect(self):
        for t in self.p:
            print t.values


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testCorrect']
    unittest.main()