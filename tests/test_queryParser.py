# -*- coding: utf-8 -*-
from unittest import TestCase
from db_exceptions import NotSupportedQuery
from qparser.query_parser import QueryParser

__author__ = 'Propp'

class TestQueryParser(TestCase):
    def setUp(self):
        self.p = QueryParser()

    def test_simple_relation_is_leaf(self):
        q = 'table'

        self.assertTrue(self.p.is_leaf(q))

    def test_operation_is_not_leaf(self):
        q = 'join(r1, r2)'

        self.assertFalse(self.p.is_leaf(q))

    def test_not_stripped_query_is_leaf(self):
        q = '   table '

        self.assertTrue(self.p.is_leaf(q))
        
    def test_allias_table_query_is_leaf(self):
        q = 'table allias'
        
        self.assertTrue(self.p.is_leaf(q))

    def test_parse_correct_operation(self):
        q = 'join(r1, r2)'

        self.assertTupleEqual(('join', '(r1, r2)'), self.p.parse_operation(q))

    def test_parse_incorrect_operation(self):
        q = 'abc(de'

        with self.assertRaises(NotSupportedQuery):
            self.p.parse_operation(q)
