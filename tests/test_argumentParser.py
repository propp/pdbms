# -*- coding: utf-8 -*-
from unittest import TestCase
from qparser.argument_parser import ArgumentParser
from db_exceptions import BadParameters

__author__ = 'Propp'

class TestArgumentParser(TestCase):
    def setUp(self):
        self.p = ArgumentParser()
    
    def test_parse_correct(self):
        s = '(abc)(cde)'
        self.assertListEqual(['abc', 'cde'], self.p.parse(s))
        
    def test_parse_inner(self):
        s = '(a(bc))(c(de))'
        
        self.assertListEqual(['a(bc)', 'c(de)'], self.p.parse(s))
        
    def test_parse_not_string(self):
        s = []
        
        with self.assertRaises(BadParameters):
            self.p.parse(s)
            
    def test_parse_unicode(self):
        s = u'(abc)(cde)'
        
        self.assertListEqual([u'abc', u'cde'], self.p.parse(s))
        
    def test_parse_incorrect(self):
        s = '(abc)cd(def)'
        
        with self.assertRaises(BadParameters):
            self.p.parse(s)
            
    def test_parse_incorrect_balance(self):
        s = '(abc))(cde)'
        
        with self.assertRaises(BadParameters):
            self.p.parse(s)
            
    def test_parse_incorrect_balance2(self):
        s = '(abc)(cde'
        
        with self.assertRaises(BadParameters):
            self.p.parse(s)