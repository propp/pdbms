# -*- coding: utf-8 -*-
from unittest import TestCase
from qparser.operation_parsers import OperationParser, get_query_tree
from qnode.projection import ProjectionNode
from qnode.table import TableNode

__author__ = 'Propp'

class TestOperationParser(TestCase):
    def setUp(self):
        self.p = OperationParser()

    def test_get_query_tree_simple(self):
        q = 'proj(*)(r)'
        tree = TableNode('r')

        self.assertEqual(tree, get_query_tree(q))

    def test_get_nested_query_tree(self):
        q = 'proj(*)(proj(r0.a0, r2.a1, r0.a1)(r))'

        tree = ProjectionNode(['r0.a0', 'r2.a1', 'r0.a1'])
        tree.subnode = TableNode('r')

        self.assertEqual(tree, get_query_tree(q))