'''
Created on 27.10.2012

@author: Propp
'''
import unittest
from qnode.projection import ProjectionNode
from qnode.table import TableNode
from tuple import DbTuple


class Test(unittest.TestCase):


    def setUp(self):
        self.p = ProjectionNode(['r1.a1'])
        self.p.subnode = TableNode('r1')


    def tearDown(self):
        pass


    def testCorrectData(self):
        l1 = [
            DbTuple(['r1.a1'],['0']),
            DbTuple(['r1.a1'],['2']),
            DbTuple(['r1.a1'],['43']),
        ]
        l2 = [t for t in self.p]
        self.assertListEqual(l1, l2)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testCorrectData']
    unittest.main()