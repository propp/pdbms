# -*- coding: utf-8 -*-

class BaseParser(object):
    class Meta:
        abstract = True

    import re
    op_pattern = re.compile(r'')
        
    def parse(self, args):
        raise NotImplemented