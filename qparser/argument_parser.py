# -*- coding: utf-8 -*-
from db_exceptions import BadParameters

__author__ = 'Propp'

class ArgumentParser(object):

    def parse(self, args_string):
        res = []
        start = 0
        balance = 0

        if not isinstance(args_string, (str, unicode)):
            raise BadParameters

        for i in range(0,len(args_string)):
            letter = args_string[i]
            if letter == '(':
                balance += 1

                if balance == 1:
                    start = i + 1
            elif letter == ')':
                balance -= 1

                if balance == 0:
                    res.append(args_string[start:i])
            else:
                if balance <= 0:
                    raise BadParameters
                
        if balance != 0:
            raise BadParameters
                
        return res