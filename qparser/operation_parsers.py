# -*- coding: utf-8 -*-
from argument_parser import ArgumentParser
from db_exceptions import BadParameters, NotSupportedQuery
from fields_parser import FieldsParser
from qnode.condition import ConditionNode
from qnode.join import JoinNode
from qnode.projection import ProjectionNode
from qnode.table import TableNode
from qparser.query_parser import QueryParser
from qnode.parallel import ParallelNode

__author__ = 'Propp'

class OperationParser(object):
    class Meta:
        abstract = True

    args_num = 2

    def parse(self, args):
        args_list = self._parse_args(args)

        return self.get_node(args_list)

    def _parse_args(self, args):
        args_list = (ArgumentParser()).parse(args)
        if len(args_list) != self.args_num:
            raise BadParameters('Must be %s parameters' % self.args_num)
        return args_list


class ProjectionParser(OperationParser):
    def get_node(self, args_list):
        fields = FieldsParser().parse(args_list[0])

        if not fields:
            return get_query_tree(args_list[1])

        node = ProjectionNode(fields)
        node.subnode = get_query_tree(args_list[1])
        return node

class JoinParser(OperationParser):
    args_num = 1

    def get_node(self, args_list):
        tables = [s.strip() for s in args_list[0].split(',')]
        if len(tables) != 2:
            raise BadParameters("Must be 2 relation in Join operation")
        if tables[0] == tables[1]:
            raise BadParameters("Relations must not to be same")
        node = JoinNode()
        node.lft = get_query_tree(tables[0])
        node.rht = ParallelNode()
        node.rht.subnode = get_query_tree(tables[1])
        return node

class ConditionParser(OperationParser):
    def get_node(self, args_list):
        node = ConditionNode(args_list[0])
        node.subnode = get_query_tree(args_list[1])
        return node

class TableParser(OperationParser):
    def parse(self, args):
        parameters = [s.strip() for s in args.split() if s]
        if len(parameters) > 2:
            raise BadParameters("'%s' is incorrect table defenition" % args)
        elif len(parameters) == 2:
            return TableNode(parameters[0], parameters[1])
        elif len(parameters) == 1:
            return TableNode(parameters[0])


def get_query_tree(query):
    if not isinstance(query, (str, unicode)):
        raise BadParameters

    qp = QueryParser()

    if qp.is_leaf(query):
        return (TableParser()).parse(query)

    operation, args = qp.parse_operation(query)

    operation_parser = get_operation_parser(operation)()

    return operation_parser.parse(args)

def get_operation_parser(operation_title):
    if operation_title == 'proj':
        return ProjectionParser
    elif operation_title == 'join':
        return JoinParser
    elif operation_title == 'cond':
        return ConditionParser

    raise NotSupportedQuery
