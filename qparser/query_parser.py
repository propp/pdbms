# -*- coding: utf-8 -*-
'''
Created on 22.09.2012

@author: Propp
'''
from db_exceptions import NotSupportedQuery

class QueryParser(object):
    '''
    classdocs
    '''
    import re
    op_pattern = re.compile(r'^(?P<operation>\w+)[ \t]*(?P<args>\((.+)\)){1,2}$', re.I)
    tab_pattern = re.compile(r'^\w+([ ]+\w+)?$', re.I)

    def is_leaf(self, query):
        return bool(self.tab_pattern.match(query.strip()))

    def parse_operation(self, query):
        matches = self.op_pattern.match(query.strip())

        if not matches:
            raise NotSupportedQuery(u'Query format is unknown')

        return matches.group('operation'), matches.group('args')
