# -*- coding: utf-8 -*-
import re
from db_exceptions import BadParameters

__author__ = 'Propp'

class FieldsParser(object):
    field_pattern = re.compile(r'^(?P<rel>\w+)\.(?P<field>\w+)$', re.I)

    def parse(self, fields):
        fields = fields.strip()

        if not fields:
            raise BadParameters('Fields list must not be empty!')

        if fields.strip() == '*':
            return None
#        res = {}

        field_list = [s.strip() for s in fields.split(',')]

        for field in field_list:
            if not self.field_pattern.match(field):
                raise BadParameters('Incorrect field: %s' % field)

        return field_list







