# -*- coding: utf-8 -*-

class BadParameters(Exception):
    pass


class EmptyQuery(Exception):
    pass


class NotSupportedQuery(Exception):
    pass