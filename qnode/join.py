# -*- coding: utf-8 -*-
from qnode import BinaryNode
from tuple import DbTuple, NullTuple

__author__ = 'Propp'


class JoinNode(BinaryNode):
    def next(self):
        for lft in self.lft:
            if isinstance(lft, NullTuple):
                break
            for rht in self.rht:
                if isinstance(rht, NullTuple):
                    break
                yield DbTuple(fields=lft.fields + rht.fields,
                    values=lft.values + rht.values)
                
        yield NullTuple()

