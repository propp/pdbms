# -*- coding: utf-8 -*-
__author__ = 'Propp'

class BaseNode(object):
    class Meta:
        abstract = True

    def next(self):
        raise NotImplemented

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __iter__(self):
        return self.next()


class SingleNode(BaseNode):
    class Meta:
        abstract = True

    subnode = None

class BinaryNode(BaseNode):
    class Meta:
        abstract = True

    lft = None
    rht = None
