# -*- coding: utf-8 -*-
import codecs
import csv
import os
import pypar
from db_exceptions import BadParameters
from qnode import BaseNode
import settings
from tuple import DbTuple, NullTuple

__author__ = 'Propp'

class TableNode(BaseNode):
    title = ''
    _fields = []

    def __init__(self, title, alias=None):
        if not isinstance(title, (str, unicode)):
            raise BadParameters
        self.title = alias if alias else title
        base_path = settings.DB_FILES_BASE_PATH
        rank = pypar.rank()
        db_filename = 'dbfiles/%s.%s.db' % (title,rank)
        db_path = os.path.join(base_path, db_filename)

        self._fp = codecs.open(db_path, "r", encoding="utf-8")
        self.reader = csv.reader(self._fp, delimiter=',',quotechar='"')

        self._fields = ["%s.%s" % (self.title, s) for s in self.reader.next()]

    def next(self):
        for db_tuple in self.reader:
            t = DbTuple()
            t.fields = self._fields
            t.values = db_tuple

            yield t
        
        self._fp.seek(0)
        self.reader.next()
        
        yield NullTuple()

    def __eq__(self, other):
        return self.title == other.title and self._fields == other._fields
