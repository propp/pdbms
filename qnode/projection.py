# -*- coding: utf-8 -*-
from db_exceptions import BadParameters
from qnode import SingleNode
from tuple import DbTuple, NullTuple

__author__ = 'Propp'

class ProjectionNode(SingleNode):
    def __init__(self, field_list):
        if not isinstance(field_list, (list)):
            raise BadParameters(u'Field list must be list')

        self._fields = field_list

    def next(self):
        for tpl in self.subnode:
            if isinstance(tpl, NullTuple):
                break
            for field in self._fields:
                if field not in tpl.fields:
                    raise BadParameters("Incorrect field names")
            
            t = DbTuple()
            t.fields = self._fields
            t.values = [tpl.values[i] \
                        for i in range(len(tpl.values)) \
                        if tpl.fields[i] in self._fields]
            
            yield t
        
        yield NullTuple()
