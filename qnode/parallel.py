#-*- coding: utf-8 -*-
from qnode import SingleNode
import pypar
import itertools
from tuple import NullTuple

msg_tag = 0

class ParallelNode(SingleNode):
    def next(self):
        self._send_to_all()
        size = pypar.size()
        flags = [f for f in itertools.repeat(True,size)]
        
        while reduce(lambda x,y: x or y, flags):
            for proc in xrange(size):
                if flags[proc]:
#                    print 'test join'
                    tpl = pypar.receive(proc)
                    
                    if isinstance(tpl, NullTuple):
                        flags[proc] = False
                    else:
                        yield tpl
                        
        yield NullTuple()
    
    def _send_to_all(self):
        size = pypar.size()
        
        for tpl in self.subnode:
            for proc in xrange(size):
                pypar.send(tpl, proc)
            