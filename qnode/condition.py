# -*- coding: utf-8 -*-
from db_exceptions import BadParameters
from qnode import SingleNode
from tuple import NullTuple

__author__ = 'Propp'

class ConditionNode(SingleNode):
    def __init__(self, parameters):
        if not isinstance(parameters, (str, unicode)):
            raise BadParameters("Parameters of node must be string")

        self._condition = parameters
        

    def next(self):
        for tpl in self.subnode:
            if isinstance(tpl, NullTuple):
                break
            cond = self._condition
            for i in range(len(tpl.fields)):
                cond = cond.replace(tpl.fields[i], tpl.values[i])
            
            try:
                is_satisfy = eval(cond);
            except:
                raise BadParameters('Your condition "%s" have errors' % self._condition)
            
            if is_satisfy:
                yield tpl
            
        yield NullTuple()
            