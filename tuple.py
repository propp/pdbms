# -*- coding: utf-8 -*-
from db_exceptions import BadParameters
__author__ = 'Propp'

class DbTuple(object):
    fields = []
    values = []
    
    def __init__(self, fields=[], values=[]):
        if not isinstance(fields, list):
            raise BadParameters('Fields parameter must be list')
        self.fields = fields
        
        if not isinstance(values, list):
            raise BadParameters('Values parameter must be list')
        self.values = values
    
    def __eq__(self, other):
        return self.__dict__ == other.__dict__
    
    def is_null(self):
        return False
    
class NullTuple(DbTuple):
    def __init__(self):
        pass
    
    def is_null(self):
        return True


