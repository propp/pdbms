# -*- coding: utf-8 -*-
from db_exceptions import EmptyQuery
from qparser.operation_parsers import get_query_tree

class Query(object):
    __query = ''

    def add_query(self, query):
        self.__query = query

    def execute(self):
        if not self.__query:
            raise EmptyQuery ('Query must not be empty')
        
        return get_query_tree(self.__query)

    def clear(self):
        self.__query = ''


