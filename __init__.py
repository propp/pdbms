# -*- coding: utf-8 -*-
import pypar
from query import Query
import itertools
from tuple import DbTuple
__author__ = 'Propp'

def print_tuple(tpl, with_header=False):
    if not isinstance(tpl, DbTuple):
        raise TypeError("I print only tuples")
    if with_header:
        print ' | '.join(['%7s' % f for f in tpl.fields])
        print '-+-'.join(itertools.repeat('-'*7,len(tpl.fields)))
    print ' | '.join(['%7s' % f for f in tpl.values])

if __name__ == '__main__':
    q = Query()
    rank = pypar.rank()
    size = pypar.size()
    
    while True:
        query = ' '*100
        results = 0
        if rank == 0:
            query = raw_input()
        
        pypar.broadcast(query, 0)
        query = query.strip() 
    
        if query == 'quit':
            break
        
        q.add_query(query)
        
        try:
            tree = q.execute()
        except Exception as e:
            print 'Error: %s' % e.message
        else:
            for tpl in tree:
                if tpl.is_null():
                    break
                if not results:
                    if rank == 0:
                        print_tuple(tpl, True)
                        pypar.barrier()
                    else:
                        pypar.barrier()
                        print_tuple(tpl)
                else:
                    print_tuple(tpl)
                results += 1
        
        pypar.barrier()
        print '%s results on process %s' % (results,rank)
                    

    pypar.finalize()
